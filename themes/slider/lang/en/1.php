﻿<section id="home_01" class="home" data-index="0" data-transition="none" data-background="images/bgd.jpg?1234" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S001" data-screen-label="Home" data-screen-name="Home" data-screen-section="">
	<div class="engUk-only footerEng">
		<p>Boehringer Ingelheim Ltd UK<br>
		UK/SPRT-171048<br>
		Date of preparation: June 2017</p>
	</div>
	<div class="slideContent">
		<h2 data-i18n="indroduction_title">
			The unique mist<br>
			<span>of Respimat<sup>&#174;</sup></span><br>
			benefits asthma<br>
			<span>patients</span>
		</h2>
		<button href="#quiz_02" class="goToSlide gtmClickNavigate" data-i18n="btn_learn_more">Learn more</button>
	</div>
	<div class="bubbleHome">
		<div class="imgProduct">
			<img border="0" src='images/productHome.png' />
		</div>
		<div class="imgBubble">
			<img border="0" src='images/bubble.png' />
		</div>
		<div class="whiteBubble first"></div>
		<div class="whiteBubble second"></div>
	</div>
</section>