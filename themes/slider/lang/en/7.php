﻿<section id="learn-more_06" class="learn-more" data-index="6" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S007" data-screen-label="Learn more" data-screen-name="Learn more" data-screen-section="">
	<div class="contentPage">
		<div class="title-anim">
			<h2 data-i18n="page_title">
				 Do you want to learn more how<br>
				<span class="blue">SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> can help your asthma patients?</span>
			</h2>
			<div class="questions">
				<div class="col">
					<h3 data-i18n="title1">Where can I find out more about the Respimat<sup>&#174;</sup> device?</h3>
				</div>
				<div class="col">
					<h3 data-i18n="title2">What else can<br>SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> do for asthma patients?</h3>
				</div>
			</div>
		</div>
		<div id="bubbleBlock">
			<div class="col firstBubble">
				<a class="gtmClickExternalLink" href="#">
					<span data-i18n="find_out_button">Find out more here</span>
					<img src="image/find-product.png" alt="find product" />
				</a>
				<p class="ersOnly">Case description for educational purposes; not real patient case<br><br>
				Summary of Product Characteristics available at the booth.<br>
				Submitted to AIFA on XX/XX/XXXX</p>
			</div>
			<div class="col secondBubble">
				<a class="jamesImage gtmClickExternalLink" data-link-type="minisite" href="spiriva/edetail1;#/james_02">
					<img src="image/meet-james.png" alt="meet-james" />
				</a>
				<a class="karenImage gtmClickExternalLink" data-link-type="minisite" href="spiriva/edetail1;#/karen_03">
					<img src="image/meet-karen.png" alt="meet-karen" />
				</a>
				<a class="jamesButton button gtmClickExternalLink" href="spiriva/edetail1;#/james_02" data-i18n="meet_james_button">Meet James</a>
				<a class="karenButton button gtmClickExternalLink" href="spiriva/edetail1;#/karen_03" data-i18n="meet_karen_button">Meet Karen</a>
			</div>
			<div class="col thirdBubble">
				<a class="juliaImage gtmClickExternalLink" href="#">
					<img src="image/meet-julia.png" alt="meet-julia" />
				</a>
				<a class="samImage gtmClickExternalLink" href="#">
					<img src="image/meet-sam.png" alt="meet-sam" />
				</a>
				<a class="juliaButton button gtmClickExternalLink" href="#" data-i18n="meet_julia_button">Meet Julia</a>
				<a class="samButton button gtmClickExternalLink" href="#" data-i18n="meet_sam_button">Meet Sam</a>
			</div>
		</div>
	</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="bgdBubble small">
	</div>

	<div class="lightbox" id="referencesPopin">
		<h2 data-i18n="title_popin_1">References</h2>
		<ol>
			<li data-i18n="list_item_popin1">Anderson P. Use of Respimat<sup>&#174;</sup> Soft Mist<sup>&#153;</sup> Inhaler in COPD patients. Int J COPD. 2006;1(3):251-9.</li>
			<li data-i18n="list_item_popin2">Pitcairn G, Reader S, Pavia D, Newman S. Deposition of corticosteroid aerosol in the human lung by Respimat Soft Mist inhaler compared to deposition by metered dose inhaler or by Turbuhaler dry powder inhaler. J Aerosol Med. 2005; 18(3):264-72.</li>
			<li data-i18n="list_item_popin3">Kerstjens H, Engel M, Dahl R, et al. Titropium in asthma poorly controlled with standard combination therapy. N Engl J Med 2012;367(13):1198-1207.</li>
			<li data-i18n="list_item_popin4">Kerstjens HAM, Moroni-Zentgraf P, Tashkin DP et al. Tiotropium improves lung function, exacerbation rate, and asthma control, independent of baseline characteristics including age, degree of airway obstruction, and allergic status. Respir Med 2016; 117:198-206.</li>
			<li data-i18n="list_item_popin5">SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> Summary of Product Characteristics. Boehringer Ingelheim International GmbH.</li>
			<li data-i18n="list_item_popin6">Demoly P, Annunziata K, Gubba E et al. Repeated cross-sectional survey of patient-reported asthma control in Europe in the past 5 years. Eur Respir Rev 2012;21(123):66-74.</li>
			<li class="hidden" data-i18n="list_item_popin7">Dahl R, Engel M, Dusser D, et al. Safety and tolerability of once-daily tiotropium respimat<sup>&#174;</sup> as add-on to at least inhaled corticosteroids in adult patients with symptomatic asthma: A pooled safety analysis. Respir Med. 2016;118:102-111.</li>
			<li class="hidden engUk-only">Global Initiative for Asthma. Pocket guide for asthma management and prevention (updated 2016). Accessed on May 30, 2017</li>

		</ol>
	</div>
	<div class="lightbox" id="prescribingPopin">
		<h2 data-i18n="title_popin_2">SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup><br>
		<span class="orange">inhalation solution product information</span></h2>
		<p data-i18n="text1_popin_2"><strong>Indications:</strong><br>
		SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> is indicated as an add-on maintenance bronchodilator treatment in adult patients with asthma who are currently treated with the maintenance combination of inhaled corticosteroids (&#8805;800 &#181;g budesonide/day or equivalent) and long-acting
		&#946;<sub>2</sub>-agonists and who experienced one or more severe exacerbations in the previous year.<sup>5</sup></p>
		<p data-i18n="text2_popin_2"><strong>Posology:</strong> <br>
		The product is intended for inhalation use with two puffs comprising one medicinal dose of 5 microgram tiotropium, not to be exceeded, once daily, at the same time each day. The full benefit with asthma will be apparent after several doses.</p>
		<p data-i18n="text3_popin_2"><strong>Safety:</strong> <br>
		An analysis of pooled data from six clinical trials of tiotropium compared with placebo in asthma patients found that, consistent with the disease profile, the most frequent adverse events were asthma, decreased peak expiratory flow rate (both less frequent with tiotropium) and nasopharyngitis. Overall incidence of dry mouth, commonly associated with use of anticholinergics, was low: tiotropium, 1.0%; placebo, 0.5%. The proportions of patients with serious adverse events were balanced across groups: tiotropium, 4.0%; placebo, 4.9%.</p>

		<div class="lightbox-button">
			<button href="#" data-i18n="btn_learn_more">Read the SPC</button>
		</div>

	</div>
</section>