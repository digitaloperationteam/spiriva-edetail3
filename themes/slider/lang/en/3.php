﻿<section id="symptoms_03" class="symptoms helpPage" data-index="2" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S003" data-screen-label="Symptoms" data-screen-name="Symptoms" data-screen-section="">
 	<div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">
					SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup><br>
					<span class="blue">improves asthma symptoms<sup>3,4</sup></span>
				</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">
						SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> significantly improved asthma symptoms vs control<sup>3,4</sup>
					</p>

					<p>
						<img border="0" src='images/master/symptomsGraph.png' />
						<span class="hidden" data-i18n="graph_element1">% of patients responding</span>
						<span class="hidden" data-i18n="graph_element2">Odds ratio=1.68 <br>P &lt; 0.05 </span>
						<span class="hidden" data-i18n="graph_element3">(205 patients)</span>
						<span class="hidden" data-i18n="graph_element4">(263 patients)</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element6">Week 48</span>
						<span class="hidden" data-i18n="graph_element7">SPIRIVA&copy; Respimat&copy; + ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element8">n=456</span>
						<span class="hidden" data-i18n="graph_element9">68% more likely to improve asthma control<sup>3,4</sup></span>
					</p>

					<p class="small" data-i18n="paragraph2">
						Analysis of overall ACQ-7 responder rate data showed a statistically
						significant improvement in asthma symptom control in patients treated with tiotropium 5 &#956;g compared
						with placebo after 24 and 48 weeks (baseline treatment &#8805; 800&#956;g budesonide or equivalent + LABA).
					</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first" >
					<div class="quote" data-i18n="text_karen">
						<p>Not only does this extra medicine feel different when I take it, but I'm coping much better at work, especially with the stairs. And I'm glad I've been able to gradually stop taking the steroid tablets.</p>
						<p class="bottom">
							<strong>Karen</strong><br>
							32 years old
						</p>
					</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_julia">
						<p>It's not nice, feeling breathless, especially when you're on your own. This extra medicine has helped me be less worried about breathlessness. I feel like I've got a bit of my life back.</p>
						<p class="bottom"><strong>Julia</strong><br>
						45 years old</p>
					</div>
				</div>
				<button href="#exacerbations_04" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Avoiding exacerbations</button>
			</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section>