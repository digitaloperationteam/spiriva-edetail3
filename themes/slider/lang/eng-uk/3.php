﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="symptoms_03" class="symptoms helpPage" data-index="2" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S003" data-screen-label="Symptoms" data-screen-name="Symptoms" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br/&gt;&lt;span class="blue"&gt;significantly improved lung function
(Peak FEV&lt;sub&gt;1(0-3h)&lt;/sub&gt;) vs placebo* in asthma&lt;sup&gt;3&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">Adapted from Kerstjens H. et al., Tiotropium in asthma poorly controlled with
standard combination therapy. New England Journal of Medicine 2012.&lt;sup&gt;3&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/symptomsGraph.png"><span class="hidden" data-i18n="graph_element1">Mean Peak FEV&lt;sub&gt;1(0-3h)&lt;/sub&gt;: change from baseline (mL)</span>
						<span class="hidden" data-i18n="graph_element2">P&lt;0.001</span>
						<span class="hidden" data-i18n="graph_element3">Placebo + ICS/LABA&lt;br/&gt;
n=203&lt;br/&gt;</span>
						<span class="hidden" data-i18n="graph_element4">(263 patients)</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + ICS/LABA&lt;br/&gt;
n=203&lt;br/&gt;</span>
						<span class="hidden" data-i18n="graph_element6">Week</span>
						<span class="hidden" data-i18n="graph_element7">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; + ICS/LABA&lt;br/&gt;
n=198&lt;br/&gt;
(95% CI 91-217; P&lt;0.001)</span>
						<span class="hidden" data-i18n="graph_element8">n=456</span>
						<span class="hidden" data-i18n="graph_element9">Up to 154mL improvement at week 24&lt;sup&gt;3&lt;/sup&gt;</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2016, Figure 4.<sup>5</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">* Placebo + at least inhaled corticosteroids (&ge;800&micro;g budesonide/day or equivalent)/long-acting &beta;&lt;sub&gt;2&lt;/sub&gt;-agonist &lt;/br&gt;
SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; was studied in two replicate RCTs in 912 asthma patients (results presented graphically are from Trial 2, n=401).&lt;sup&gt;3&lt;/sup&gt; In Trial 1, there was an improvement in peak FEV&lt;sub&gt;1&lt;/sub&gt; of 86 mL at week 24 (95% CI 20-152; p&lsaquo;0.05).&lt;sup&gt;3&lt;/sup&gt; &lt;/br&gt;
ICS: inhaled corticosteroids; LABA: long-acting &beta;&lt;sub&gt;2&lt;/sub&gt;-agonist; FEV&lt;sub&gt;1&lt;/sub&gt;: forced expiratory volume in 1 second; RCTs: randomised controlled trials; CI: confidence interval; n=number of patients who completed the study</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_karen">&lt;p&gt;I am coping much better at work, especially with the stairs. It's also been nice walking to the park with my kids again and not needing to stop - even once.&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Karen&lt;/strong&gt;&lt;br/&gt;
							32 years old
						&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_julia">&lt;p&gt;It's not nice, feeling breathless, especially when you're on your own. This extra medicine has helped me be less worried about breathlessness. I feel like I've got a bit of my life back.&lt;/p&gt;
						&lt;p class="bottom"&gt;&lt;strong&gt;Julia&lt;/strong&gt;&lt;br/&gt;
						45 years old&lt;/p&gt;</div>
				</div>
				<button href="#exacerbations_04" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Reducing exacerbation risk</button>
			</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
