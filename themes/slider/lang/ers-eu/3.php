﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="symptoms_03" class="symptoms helpPage" data-index="2" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S003" data-screen-label="Symptoms" data-screen-name="Symptoms" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;&lt;br&gt;
					&lt;span class="blue"&gt;improves asthma symptoms&lt;sup&gt;4,5&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt; significantly improved asthma symptoms vs control&lt;sup&gt;4,5&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/symptomsGraph.png"><span class="hidden" data-i18n="graph_element1">% of patients responding</span>
						<span class="hidden" data-i18n="graph_element2">Odds ratio=1.68 &lt;br&gt;P &amp;lt; 0.05</span>
						<span class="hidden" data-i18n="graph_element3">(205 patients)</span>
						<span class="hidden" data-i18n="graph_element4">(263 patients)</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element6">Week 48</span>
						<span class="hidden" data-i18n="graph_element7">SPIRIVA&amp;copy; Respimat&amp;copy; + ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element8">n=456</span>
						<span class="hidden" data-i18n="graph_element9">68% more likely to improve asthma control&lt;sup&gt;3,4&lt;/sup&gt;</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2016, Figure 4.<sup>5</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">Analysis of overall ACQ-7 responder rate data showed a statistically significant improvement in asthma symptom control in patients treated with tiotropium 5 &amp;#956;g compared with placebo after 24 and 48 weeks (baseline treatment &amp;#8805; 800&amp;#956;g budesonide or equivalent + LABA).&lt;sup&gt;5&lt;/sup&gt;
&lt;p class="ersOnly"&gt;Case description for educational purposes; not real patient case&lt;/p&gt;</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_karen">&lt;p&gt;Not only does this extra medicine feel different when I take it, but I'm coping much better at work, especially with the stairs.&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Karen&lt;/strong&gt;&lt;br&gt;
							32 years old
						&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_julia">&lt;p&gt;It's not nice, feeling breathless, especially when you're on your own. This extra medicine has helped me be less worried about breathlessness. I feel like I've got a bit of my life back.&lt;/p&gt;
						&lt;p class="bottom"&gt;&lt;strong&gt;Julia&lt;/strong&gt;&lt;br&gt;
						45 years old&lt;/p&gt;</div>
				</div>
				<button href="#exacerbations_04" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Avoiding exacerbations</button>
			</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
