﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="stay-ahead_05" class="stay-ahead productPage" data-index="4" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S005" data-screen-label="Stay ahead" data-screen-name="Stay ahead" data-screen-section=""><div class="contentPage">
		<div class="col">
			<img id="imgProductPNG" border="0" src="images/img_product.gif"><div class="product_img_shadow">
				<img src="images/spiriva-shadow.png" alt="Spiriva shadow product"></div>
		</div>
		<div class="col">
			<h2 data-i18n="pageTitle">Help your patients&lt;br&gt;
				&lt;span class="blue"&gt;Stay ahead of asthma with SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;&lt;/span&gt;</h2>
			<ul><li class="cloud"><span data-i18n="list_item1">The Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt; inhaler actively generates the unique mist of slow moving, long-lasting fine droplets that help your patients to get the medication deep into their lungs.&lt;sup&gt;1,2&lt;/sup&gt;</span>	
				</li>
				<li class="poumons"><span data-i18n="list_item2">Symptomatic asthma, despite availability of inhaled corticosteroids/long-acting &amp;#946;&lt;sub&gt;2&lt;/sub&gt;-agonists is a common condition.&lt;sup&gt;6&lt;/sup&gt; These symptoms may keep patients from physical or other daily activities. SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt; can improve symptoms and reduce the risk of severe exacerbations to help patients stay ahead of asthma.&lt;sup&gt;4,5&lt;/sup&gt;</span></li>
			</ul><p class="engUk-only">
				SPIRIVA&reg; Respimat&reg; is indicated as an add-on maintenance bronchodilator treatment in adult patients with asthma who are currently treated with the maintenance combination of inhaled corticosteroids (&ge;800 &mu;g budesonide/day or equivalent) and long-acting &beta;<sub>2</sub>-agonists<sup>5</sup> and who experienced one or more severe exacerbations in the previous year.
			</p>
			<div class="animated-button">
				<p data-i18n="paragraph1">&lt;strong&gt;To use SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;, patients just need to Turn, Open and Press.&lt;sup&gt;1&lt;/sup&gt;&lt;/strong&gt;</p>
				<button href="#how-to-use_06" class="goToSlide gtmClickNavigate" data-i18n="button_conclusion">Learn more about SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;</button>
			</div>
		</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
