﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="exacerbations_04" class="helpPage" data-index="3" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S004" data-screen-label="Exacerbations" data-screen-name="Exacerbations" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;&lt;br&gt;
					&lt;span class="blue"&gt;reduces the risk of severe exacerbations&lt;sup&gt;4,5&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt; significantly delayed the time to &lt;br&gt;first severe asthma exacerbation vs control&lt;sup&gt;4&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/exacerbationsGraph.png"><span class="hidden" data-i18n="graph_element1">Days to 1st severe asthma exacerbation</span>
						<span class="hidden" data-i18n="graph_element2">Hazard ratio=0.79 &lt;br&gt; P=0.03</span>
						<span class="hidden" data-i18n="graph_element3">226 days</span>
						<span class="hidden" data-i18n="graph_element4">226 days</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + At least ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element6">SPIRIVA&amp;copy; Respimat&amp;copy; + at least ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element7">n=454</span>
						<span class="hidden" data-i18n="graph_element8">21% risk reduction&lt;sup&gt;3&lt;/sup&gt;</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2012, Figure 2C.<sup>4</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">Severe asthma exacerbations were predefined in the clinical trial protocol as all asthma exacerbations that required treatment with systemic (including oral) corticosteroids for at least 3 days, or, in case of ongoing and pre-existing systemic corticosteroid therapy, that required at least a doubling of the previous daily dose of systemic corticosteroids for at least 3 days.&lt;sup&gt;4&lt;/sup&gt;
&lt;p class="ersOnly"&gt;Case description for educational purposes; not real patient case&lt;/p&gt;</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_james">&lt;p&gt;Now that I am on SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;,&lt;br&gt;
						I'm breathing so much better.
						&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;James&lt;/strong&gt;&lt;br&gt;
							22 years old
						&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_sam">&lt;p&gt;It felt different from the other medications and I soon realised that I didn't need my rescue inhaler anywhere near as often. The spray design makes it more pleasant to inhale.
						&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Sam&lt;/strong&gt;&lt;br&gt;
							30 years old
						&lt;/p&gt;</div>
				</div>
				<button href="#stay-ahead_05" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">What is SPIRIVA&lt;sup&gt;&amp;#174;&lt;/sup&gt; Respimat&lt;sup&gt;&amp;#174;&lt;/sup&gt;</button>
			</div>
	</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
