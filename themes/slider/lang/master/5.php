﻿<section id="stay-ahead_05" class="stay-ahead productPage" data-index="4" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S005" data-screen-label="Stay ahead" data-screen-name="Stay ahead" data-screen-section="">
	<div class="contentPage">
		<div class="col">
			<img id="imgProductPNG" border="0"  src='images/img_product.gif' />
			<div class="product_img_shadow">
				<img src="images/spiriva-shadow.png" alt="Spiriva shadow product" />
			</div>
		</div>
		<div class="col">
			<h2 data-i18n="pageTitle">
				Help your patients<br>
				<span class="blue">Stay ahead of asthma with SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup></span>
			</h2>
			<ul>
				<li class="cloud"><span data-i18n="list_item1">				
					The Respimat<sup>&#174;</sup> inhaler actively generates the unique mist of slow moving, long-lasting fine droplets that help your patients to get the medication deep into their lungs.<sup>1</sup></span>	
				</li>
				<li class="poumons"><span data-i18n="list_item2">
					Symptomatic asthma, despite daily use of inhaled corticosteroids/long-acting &#946;<sub>2</sub>-agonists is a common condition.<sup>6</sup> These symptoms may keep patients from physical or other daily activities. SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> can improve symptoms and reduce the risk of severe exacerbations to help patients stay ahead of asthma.
				</span></li>
			</ul>
			<p class="engUk-only">
				SPIRIVA&reg; Respimat&reg; is indicated as an add-on maintenance bronchodilator treatment in adult patients with asthma who are currently treated with the maintenance combination of inhaled corticosteroids (&ge;800 &mu;g budesonide/day or equivalent) and long-acting &beta;<sub>2</sub>-agonists<sup>5</sup> and who experienced one or more severe exacerbations in the previous year.
			</p>
			<div class="animated-button">
				<p data-i18n="paragraph1"><strong>To use SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup>, patients just need to Turn, Open and Press.</strong></p>
				<button href="#how-to-use_06" class="goToSlide gtmClickNavigate" data-i18n="button_conclusion">Learn more about SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup></button>
			</div>
		</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section>