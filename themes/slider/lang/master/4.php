﻿<section id="exacerbations_04" class="helpPage" data-index="3" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S004" data-screen-label="Exacerbations" data-screen-name="Exacerbations" data-screen-section="">
	<div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">
					SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup><br>
					<span class="blue">reduces the risk of severe exacerbations<sup>3,4</sup></span>
				</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">
						SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup> significantly delayed the time to <br>first severe asthma exacerbation vs control<sup>3</sup>
					</p>

					<p>
						<img border="0" src='images/master/exacerbationsGraph.png' />
						<span class="hidden" data-i18n="graph_element1">Days to 1st severe asthma exacerbation</span>
						<span class="hidden" data-i18n="graph_element2">Hazard ratio=0.79 <br> P=0.03</span>
						<span class="hidden" data-i18n="graph_element3">226 days</span>
						<span class="hidden" data-i18n="graph_element4">226 days</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + At least ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element6">SPIRIVA&copy; Respimat&copy; + at least ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element7">n=454</span>
						<span class="hidden" data-i18n="graph_element8">21% risk reduction<sup>3</sup></span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2012, Figure 2C.<sup>4</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">
						Severe asthma exacerbations were predefined in the clinical trial protocol as all asthma exacerbations that required treatment with systemic (including oral) corticosteroids for at least 3 days, or, in case of ongoing and pre-existing systemic corticosteroid therapy, that required at least a doubling of the previous daily dose of systemic corticosteroids for at least 3 days.<sup>3</sup>
					</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first" >
					<div class="quote" data-i18n="text_james">
						<p>Now that I am on SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup>,<br>
						I'm breathing so much better.
						</p>
						<p class="bottom">
							<strong>James</strong><br>
							22 years old
						</p>
					</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_sam">
						<p>It felt different from the other medications and I soon realised that I didn't need my rescue inhaler anywhere near as often. The spray design makes it more pleasant to inhale.
						</p>
						<p class="bottom">
							<strong>Sam</strong><br>
							30 years old
						</p>
					</div>
				</div>
				<button href="#stay-ahead_05" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">What is SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup></button>
			</div>
	</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section>