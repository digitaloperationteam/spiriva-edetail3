﻿<section id="how-to-use_06" class="how-to-use" data-index="5" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S006" data-screen-label="How to use" data-screen-name="How to use" data-screen-section="">

<div class="contentPage">

	<div class="col left-side">
		<div class="joiner-point"> 		
		</div>
		<div class="radial-inner">
		</div>
		<div class="radial-background"></div>
		<ul class="radial-slide-block">

			<li id="turn" class="use-element zoom-in">
				<p class="number">1</p>
				<div class="slide-info" style="opacity:1; display: block;">
					<p data-i18n="how_to_use1">TURN</p>
					<p data-i18n="how_to_use2">Hold the inhaler upright with the cap closed</p>
					<p data-i18n="how_to_use3">Turn the base of the inhaler in the direction of the arrows until you hear a click.<sup>5</sup></p>
				</div>
			</li>
			<li id="open" class="use-element active">
				<p class="number">2</p>
				<div class="slide-info">
					<p data-i18n="how_to_use4">OPEN</p>
					<p data-i18n="how_to_use5">Snap back the lid.</p>
					<p data-i18n="how_to_use6">Open the cap until it snaps fully open.<sup>5</sup></p>
				</div>
			</li>
			<li id="press" class="use-element active">
					<p class="number">3</p>
				<div class="slide-info">
					<p data-i18n="how_to_use7">PRESS</p>
					<p data-i18n="how_to_use8">Breathe out slowly and fully.</p>
					<p data-i18n="how_to_use9">Close your lips around the mouthpiece. While taking a slow, deep breath, press the dose release button and continue to breath in<sup>5</sup></p>
				</div>
			</li>
			<li id="repeat" class="use-element active">
					<p class="number">4</p>
				<div class="slide-info">
					<p data-i18n="how_to_use10">REPEAT</p>
					<p data-i18n="how_to_use11">Turn, Open, Press.</p>
					<p data-i18n="how_to_use12">For a total of 2 puffs.<sup>5</sup></p>
				</div>
			</li>

		</ul>
	</div>

	 <div class="col right-side">
	 	<h2 data-i18n="how_to_use_title">
	 		Daily use of SPIRIVA<sup>&#174;</sup> Respimat<sup>&#174;</sup>:<br>
	 		<span class="blue">Turn - Open - Press</span>
	 	</h2>
	 	<p data-i18n="how_to_use_paragraph">Please ensure that your patients know how to use the Respimat<sup>&#174;</sup> properly. Advise your patients to:</p>
	 	<button data-i18n="how_to_use-button" href="#learn-more_06" class="goToSlide gtmClickNavigate">Find out more</button>
	 </div>

</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>

</section>