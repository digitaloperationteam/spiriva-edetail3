﻿<section id="quiz_02" data-background="images/bgd.jpg?1234" data-index="1" class="quiz" data-transition="none" data-screen-id="2016_SPI_ED3_S002" data-screen-label="Quiz" data-screen-name="Quiz" data-screen-section="">
	<div class="contentPage ersNot">
		<h2 data-i18n="page_title">
			Respimat<sup>&#174;</sup>: the unique mist<br>
			<span class="blue">that makes its way deep into patients' lungs<sup>1</sup></span>
		</h2>
		<p data-i18n="quiz_indication" class="hiden-on-answer">Estimate how much medication (mean dose deposition) reaches a patient's lungs with each device<sup>1,2</sup>.<br>Please drag the orange droplet to the percentage of medication you consider appropriate, and press on 'check answers' to reveal the answers.</p>
		<div class="lungSystem">
			<div id="contentLung1" class="contentCircle">
				<div id="lung1" class="dragItem circle">
					<span class="lung step1"></span>
					<span class="lung step2"></span>
					<span class="lung step3"></span>
					<span class="lung step4"></span>
					<span class="lung step5"></span>
					<span class="lung step6"></span>
					<span class="lung step7"></span>
					<span class="lung step8"></span>
					<span class="lung step9"></span>
					<span class="lung step10"></span>
					<span class="lung step11"></span>
					
				</div>
				<div class="circleProgress pourcentage-0">
					<div class="bar"></div>
					<div class="fill"></div>
				</div>
				<div id="sliderLung1" class="slider" data-target="contentLung1" >
					<span class="startPourcentage">0%</span>
					<span class="endPourcentage">100%</span>
				</div>
				<h3 data-i18n="quiz1_title" >CFC-pMDI<sup>*</sup></h3>
	        </div>
	        <div id="contentLung2" class="contentCircle">
				<div id="lung2" class="dragItem circle">
					<span class="lung step1"></span>
					<span class="lung step2"></span>
					<span class="lung step3"></span>
					<span class="lung step4"></span>
					<span class="lung step5"></span>
					<span class="lung step6"></span>
					<span class="lung step7"></span>
					<span class="lung step8"></span>
					<span class="lung step9"></span>
					<span class="lung step10"></span>
					<span class="lung step11"></span>
					
				</div>
				<div class="circleProgress pourcentage-0">
					<div class="bar"></div>
					<div class="fill"></div>
				</div>
				<div id="sliderLung2" class="slider" data-target="contentLung2">
					<span class="startPourcentage">0%</span>
					<span class="endPourcentage">100%</span>
				</div>
				<h3 data-i18n="quiz2_title">DPI<sup>&dagger;</sup> - fast inhalation</h3>
	        </div>
	        <div id="contentLung3" class="contentCircle">
				<div id="lung3" class="dragItem circle">
					<span class="lung step1"></span>
					<span class="lung step2"></span>
					<span class="lung step3"></span>
					<span class="lung step4"></span>
					<span class="lung step5"></span>
					<span class="lung step6"></span>
					<span class="lung step7"></span>
					<span class="lung step8"></span>
					<span class="lung step9"></span>
					<span class="lung step10"></span>
					<span class="lung step11"></span>
					
				</div>
				<div class="circleProgress pourcentage-0">
					<div class="bar"></div>
					<div class="fill"></div>
				</div>
				<div id="sliderLung3" class="slider" data-target="contentLung3">
					<span class="startPourcentage">0%</span>
					<span class="endPourcentage">100%</span>
				</div>
				<h3 data-i18n="quiz3_title">Respimat<sup>&#174;</sup></h3>
	        </div>
        </div>
    	<div id="answer" class="hidden">
			<div><img src="images/answer_data.png" alt=""></div>
			<span class="hidden" data-i18n="graph_element1">Mean dose deposited</span>
			<span class="hidden" data-i18n="graph_element2">Oropharynx</span>
			<span class="hidden" data-i18n="graph_element3">Lungs</span>
		</div>
        <div class="twoCol">
    		<p class="nograph engUk-only small">Results are based on data taken from Andersen P et al and Pitcairn G et al.<sup>1,2</sup> Images corresponding to intervening levels are extrapolated from the published data.</p>
    		<p class="onlygraph engUk-only small">Results are based on data taken from Andersen P et al and Pitcairn G et al.<sup>2</sup></p>
        	<div class="col">
        		
        		<button data-gtm="SPC" data-featherlight="#quizSpcEnguk" class="gtmClickPopin engUk-only">Study design</button>
        		<p data-i18n="bottom_text" class="small">
        			Images displayed with correct answers are from cited references;<br>
					images corresponding to intervening levels are extrapolated from the published data. <br>
					<sup>*</sup>metered dose inhaler <br>
					<sup>&dagger;</sup>dry powder inhaler
        		</p>
        	</div>
        	<div class="col buttonSet1">
        		<button href="#" class="goToSlide checkAnswer gtmClickQuizShowAnswer" data-i18n="button_check-anwsers">Check answers</button>
        	</div>
        	<div class="col buttonSet2 hidden">
        		<button href="#symptoms_03" class="goToSlide gtmClickNavigate" data-index="2" data-i18n="button_managing">Managing symptomatic asthma</button>
        		<a class="jsSeeData gtmClickNavigate" data-gtm="See data" data-i18n="link_see_data">See the data</a>
        		<a class="jsSeeVisual hidden gtmClickNavigate" data-gtm="See the visual" data-i18n="link_see_visual">See the visual</a> 
        	</div>
        </div>
	</div>

	<div class="contentPage ersOnly">
		<h2 data-i18n="page_title">
			Respimat<sup>&#174;</sup>: the unique mist<br>
			<span class="blue">that makes its way deep into patients' lungs<sup>2,3</sup></span>
		</h2>
		<p class="hide-onlyfor-ers"><strong>Table 1</strong> Mean percentage dose of steroid deposited (and range) for different devices in asthmatic subjects (modified from Pitcairn et al 2005), adapted from Anderson, 2006, Table 1.<sup>2</sup></p>
		<p class="hiden-on-answer"><strong>Table 1</strong> Mean percentage dose of steroid deposited (and range) for different devices in asthmatic subjects (modified from Pitcairn et al 2005), adapted from Anderson, 2006, Table 1.<sup>2</sup></p>
		<div class="lungSystem">
			<div id="contentLung1" class="contentCircle">
				<div id="lung1" class="circle">
					<span class="lung1 step"></span>
				</div>
				<h3>pMDI<sup>*</sup></h3>
	        </div>
	        <div id="contentLung2" class="contentCircle">
				<div id="lung2" class="circle">
					<span class="lung2 step"></span>
				</div>
				<h3>Turbohaler DPI<sup>&#8224;</sup> - slow</h3>
	        </div>
	        <div id="contentLung3" class="contentCircle">
				<div id="lung3" class="circle">
					<span class="lung3 step"></span>
				</div>
				<h3>Turbohaler DPI<sup>&#8224;</sup> - fast</h3>
	        </div>
	        <div id="contentLung4" class="contentCircle">
				<div id="lung4" class="circle">
					<span class="lung4 step"></span>
				</div>
				<h3>Respimat<sup>&#174;</sup> SMI</h3>
	        </div>
        </div>
    	<div id="answer" class="hidden">
			<div><img src="images/answer_data.png" alt=""></div>
			<span class="hidden">Mean dose deposited</span>
			<span class="hidden">Oropharynx</span>
			<span class="hidden">Lungs</span>
		</div>
        <div class="twoCol">
        	<div class="col">
        		<button data-gtm="SPC" data-featherlight="#quizSpcEnguk" class="gtmClickPopin engUk-only">Study design</button>
        		<p data-i18n="bottom_text" class="small">
        			Images displayed with correct answers are from cited references;<br>
					<br>
					<sup>*</sup>metered dose inhaler <br>
					<sup>&dagger;</sup>dry powder inhaler
        		</p>
        	</div>
        	<div class="col buttonSet2">
        		<button href="#symptoms_03" class="goToSlide gtmClickNavigate" data-index="2" data-i18n="button_managing">Managing symptomatic asthma</button>
        		<a class="jsSeeData gtmClickNavigate" data-gtm="See data" data-i18n="link_see_data">See the data</a>
        		<a class="jsSeeVisual hidden gtmClickNavigate" data-gtm="See the visual" data-i18n="link_see_visual">See the visual</a> 
        	</div>
        </div>
	</div>

	<div class="lightbox" id="quizSpcEnguk">
		<h2>Study design</h2>
		<!--<p>Images displayed with correct answers are based on result data taken from Anderson P. et al., Use of Respimat<sup>&#174;</sup> Soft Mist&trade; Inhaler in COPD patients. International Journal of Chronic Obstructive Pulmonary Disease, 2006; Pitcairn G. et al., Deposition of corticosteroid aerosol in the human lung by Respimat Soft Mist inhaler compared to deposition by metered dose inhaler or by Turbuhaler dry powder inhaler. Journal of Aerosol Medicine and Pulmonary Drug Delivery, 2005.<br>
		Images corresponding to intervening levels are extrapolated from the published data.</p>-->
		<p>Fourteen mild-to-moderate asthmatic patients completed a randomized four-way crossover scintigraphic study to determine the lung deposition of 200 mcg budesonide inhaled from a Respimat Soft Mist Inhaler (Respimat SMI), 200 mcg budesonide inhaled from a Turbuhaler dry powder inhaler (Turbuhaler DPI, used with fast and slow peak inhaled flow rates), and 250 mcg beclomethasone dipropionate inhaled from a pressurized metered dose inhaler (Becloforte pMDI). Mean (range) whole lung deposition of drug from the Respimat SMI (51.6 [46-57]% of the metered dose) was significantly (p&nbsp;&lt; 0.001) greater than that from the Turbuhaler DPI used with both fast and slow inhaled flow rates (28.5 [24-33]% and 17.8 [14-22]%, respectively) or from the Becloforte pMDI (8.9 [6-12]%).<sup>1,2</sup></p>
	</div>

</section>
