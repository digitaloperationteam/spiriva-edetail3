﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="exacerbations_04" class="helpPage" data-index="3" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S004" data-screen-label="Exacerbations" data-screen-name="Exacerbations" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br/&gt;&lt;span class="blue"&gt;redujo significativamente el riesgo de exacerbaciones asm&aacute;ticas graves&lt;sup&gt;3,4&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; retras&oacute; significativamente el tiempo hasta la primera exacerbaci&oacute;n asm&aacute;tica grave en comparaci&oacute;n con el tratamiento de referencia&lt;sup&gt;3&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/exacerbationsGraph.png"><span class="hidden" data-i18n="graph_element1">D&iacute;as hasta la primera exacerbaci&oacute;n asm&aacute;tica grave</span>
						<span class="hidden" data-i18n="graph_element2">HR&nbsp;=&nbsp;0,79 &lt;br/&gt; P=0,03</span>
						<span class="hidden" data-i18n="graph_element3">226 d&iacute;as</span>
						<span class="hidden" data-i18n="graph_element4">282 d&iacute;as</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + LABA/ICS</span>
						<span class="hidden" data-i18n="graph_element6">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt; + at least ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element7">n=454</span>
						<span class="hidden" data-i18n="graph_element8">21% reducci&oacute;n del riesgo&lt;sup&gt;3&lt;/sup&gt;.</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2012, Figure 2C.<sup>4</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">En el protocolo de ensayo cl&iacute;nico se predefinieron las exacerbaciones asm&aacute;ticas graves como todas aquellas que requirieran tratamiento con corticoesteroides sist&eacute;micos (incluyendo por v&iacute;a oral) durante al menos 3 d&iacute;as o, en caso de tratamiento con corticoesteroides sist&eacute;micos en curso y preexistente, que requirieran al menos doblar la dosis diaria previa de corticoesteroides sist&eacute;micos durante por lo menos 3 d&iacute;as.&lt;sup&gt;3&lt;/sup&gt;</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_james">&lt;p&gt;Ahora que utilizo SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;,&lt;br/&gt;
						respiro mucho mejor.
						&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Luis&lt;/strong&gt;&lt;br/&gt;
							22 a&ntilde;os
						&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_sam">&lt;p&gt;La sensaci&oacute;n era diferente a la de otros medicamentos y pronto descubr&iacute; que ya no necesitaba el inhalador de rescate ni la mitad de veces que antes. La inhalaci&oacute;n es m&aacute;s agradable gracias al dise&ntilde;o en forma de espray.
						&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Sara&lt;/strong&gt;&lt;br/&gt;
							30 a&ntilde;os
						&lt;/p&gt;</div>
				</div>
				<button href="#stay-ahead_05" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Qu&eacute; es SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;</button>
			</div>
	</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
