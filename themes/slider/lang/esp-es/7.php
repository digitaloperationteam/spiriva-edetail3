﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="learn-more_06" class="learn-more" data-index="6" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S007" data-screen-label="Learn more" data-screen-name="Learn more" data-screen-section=""><div class="contentPage">
		<div class="title-anim">
			<h2 data-i18n="page_title">&iquest;Desea saber m&aacute;s sobre c&oacute;mo&lt;br/&gt;&lt;span class="blue"&gt;SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; puede ayudar a sus pacientes asm&aacute;ticos?&lt;/span&gt;</h2>
			<div class="questions">
				<div class="col">
					<h3 data-i18n="title1">&iquest;D&oacute;nde puedo encontrar m&aacute;s informaci&oacute;n sobre el dispositivo Respimat&lt;sup&gt;&reg;&lt;/sup&gt;?</h3>
				</div>
				<div class="col">
					<h3 data-i18n="title2">&iquest;Qu&eacute; m&aacute;s puede hacer &lt;br/&gt;SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; por los pacientes asm&aacute;ticos?</h3>
				</div>
			</div>
		</div>
		<div id="bubbleBlock">
			<div class="col firstBubble">
				<a class="gtmClickExternalLink" data-gtm="Product informations" data-i18n="[href]find_out_btn" href="https://www.avancesenrespiratorio.com/asma_opciones_terapeuticas&amp;tipo=pro" target="_blank">
					<span data-i18n="find_out_button">M&aacute;s informaci&oacute;n aqu&iacute;</span>
					<img src="image/find-product.png" alt="find product"></a>
				<p class="ersOnly">Case description for educational purposes; not real patient case<br><br>
				Summary of Product Characteristics available at the booth.<br>
				Submitted to AIFA on 01/08/2017</p>
			</div>
			<div class="col secondBubble">
				<a target="_blank" class="jamesImage gtmClickExternalLink" data-link-type="minisite" data-gtm="James-image" href="spiriva/edetail1;#/james_02">
					<img src="image/meet-james.png" alt="meet-james"></a>
				<a target="_blank" class="karenImage gtmClickExternalLink" data-link-type="minisite" data-gtm="Karen-image" href="spiriva/edetail1;#/karen_03">
					<img src="image/meet-karen.png" alt="meet-karen"></a>
				<a target="_blank" class="jamesButton button gtmClickExternalLink" data-link-type="minisite" data-gtm="James-button" href="spiriva/edetail1;#/james_02" data-i18n="meet_james_button">Conozca a Luis</a>
				<a target="_blank" class="karenButton button gtmClickExternalLink" data-link-type="minisite" data-gtm="Karen-button" href="spiriva/edetail1;#/karen_03" data-i18n="meet_karen_button">Conozca a Carina</a>
			</div>
			<div class="col thirdBubble">
				<a target="_blank" class="juliaImage gtmClickExternalLink" data-gtm="Julia-image" data-link-type="minisite" href="spiriva/edetail2;#/julia_03">
					<img src="image/meet-julia.png" alt="meet-julia"></a>
				<a target="_blank" class="samImage gtmClickExternalLink" data-gtm="Sam-image" data-link-type="minisite" href="spiriva/edetail2;#/sam_02">
					<img src="image/meet-sam.png" alt="meet-sam"></a>
				<a target="_blank" class="juliaButton button gtmClickExternalLink" data-gtm="Julia-button" data-link-type="minisite" href="spiriva/edetail2;#/julia_03" data-i18n="meet_julia_button">Conozca a Julia</a>
				<a target="_blank" class="samButton button gtmClickExternalLink" data-gtm="Sam-button" data-link-type="minisite" href="spiriva/edetail2;#/sam_02" data-i18n="meet_sam_button">Conozca a Sara</a>
			</div>
		</div>
	</div>
	<div class="jobCode espEsOnly">
		SPI.1584.072017 - Boehringer Ingelheim Espa&ntilde;a, S.A. - Fecha de preparaci&oacute;n: Julio 2017
	</div>
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="bgdBubble small">
	</div>

	<div class="lightbox" id="referencesPopin">
		<h2 data-i18n="title_popin_1">Referencias</h2>
		<ol><li data-i18n="list_item_popin1">Anderson P. Use of Respimat&lt;sup&gt;&reg;&lt;/sup&gt; Soft Mist&lt;sup&gt;&#153;&lt;/sup&gt; Inhaler in COPD patients. Int J COPD. 2006;1(3):251-9.</li>
			<li data-i18n="list_item_popin2">Pitcairn G, Reader S, Pavia D, Newman S. Deposition of corticosteroid aerosol in the human lung by Respimat Soft Mist inhaler compared to deposition by metered dose inhaler or by Turbuhaler dry powder inhaler. J Aerosol Med. 2005; 18(3):264-72.</li>
			<li data-i18n="list_item_popin3">Kerstjens H, Engel M, Dahl R, et al. Tiotropium in asthma poorly controlled with standard combination therapy. N Engl J Med 2012;367(13):1198-1207.</li>
			<li data-i18n="list_item_popin4">Kerstjens HAM, Moroni-Zentgraf P, Tashkin DP et al. Tiotropium improves lung function, exacerbation rate, and asthma control, independent of baseline characteristics including age, degree of airway obstruction, and allergic status. Respir Med 2016; 117:198-206.</li>
			<li data-i18n="list_item_popin5">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; Summary of Product Characteristics. Boehringer Ingelheim International GmbH.</li>
			<li data-i18n="list_item_popin6">Demoly P, Annunziata K, Gubba E et al. Repeated cross-sectional survey of patient-reported asthma control in Europe in the past 5 years. Eur Respir Rev 2012;21(123):66-74.</li>
			<li class="engGlo-only" data-i18n="list_item_popin7">Dahl R, Engel M, Dusser D, et al. Safety and tolerability of once-daily tiotropium respimat&lt;sup&gt;&reg;&lt;/sup&gt; as add-on to at least inhaled corticosteroids in adult patients with symptomatic asthma: A pooled safety analysis. Respir Med. 2016;118:102-111.</li>
			<li class="hidden engUk-only">Global Initiative for Asthma. Pocket guide for asthma management and prevention (updated 2016). Accessed October 2017</li>
			<li class="ersOnly">Placebo Respimat<sup>&reg;</sup> inhaler package leaflet; instructions for demonstration. Boehringer Ingelheim International GmbH. July 2016</li>
		</ol></div>
	<div class="lightbox" id="prescribingPopin">
		<h2 data-i18n="title_popin_2">Informaci&oacute;n del producto  SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br/&gt;&lt;span class="orange"&gt;soluci&oacute;n para inhalaci&oacute;n &lt;/span&gt;</h2>
		<p data-i18n="text1_popin_2">&lt;strong&gt;Indicaciones:&lt;/strong&gt;&lt;br/&gt;
		SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;est&aacute; indicado como tratamiento broncodilatador de mantenimiento adicional en pacientes adultos con asma que ya est&aacute;n en tratamiento con la combinaci&oacute;n de LABA/ICS y que experimentaron una o m&aacute;s exacerbaciones asm&aacute;ticas graves en el a&ntilde;o anterior.</p>
		<p data-i18n="text2_popin_2">&lt;strong&gt;Posolog&iacute;a:&lt;/strong&gt; &lt;br/&gt;
		El producto debe administrarse en dos inhalaciones que constituyen una dosis de 5&nbsp;microgramos de tiotropio una vez al d&iacute;a. El efecto completo para el paciente asm&aacute;tico ser&aacute; percibido despu&eacute;s de varias dosis de administraci&oacute;n.</p>
		<p data-i18n="text3_popin_2">&lt;strong&gt;Seguridad:&lt;/strong&gt; &lt;br/&gt;
		El efecto secundario m&aacute;s frecuente en 6 ensayos cl&iacute;nicos controlados con placebo en pacientes con asma (1256 pacientes) con periodos de tratamiento que van desde cuatro semanas a un a&ntilde;o fue la sequedad de boca (un 1,2&nbsp;%). No se notificaron interrupciones del tratamiento debidas a la sequedad de boca. Son efectos indeseables graves coherentes con los efectos anticolin&eacute;rgicos de SPIRIVA&reg; s&iacute;ntomas como glaucoma, estre&ntilde;imiento, obstrucci&oacute;n intestinal (incluido el &iacute;leo paral&iacute;tico) y retenci&oacute;n urinaria.</p>

		<div class="lightbox-button">
			<a class="button gtmClickDownload" href="http://comms.univadis.com/p/BI/spiriva/FI_Spiriva_Respimat_ES.pdf" data-gtm="PI" data-i18n="btn_popin_learn_more;[href]btn_popin_learn_more_url">Ficha T&eacute;cnica</a>
		</div>

	</div>
</section></body></html>
