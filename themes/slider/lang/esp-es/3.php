﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="symptoms_03" class="symptoms helpPage" data-index="2" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S003" data-screen-label="Symptoms" data-screen-name="Symptoms" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br/&gt;&lt;span class="blue"&gt;mejora los s&iacute;ntomas del asma&lt;sup&gt;3,4&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; mejor&oacute; los s&iacute;ntomas del asma frente al control&lt;sup&gt;3,4&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/symptomsGraph.png"><span class="hidden" data-i18n="graph_element1">% de pacientes que respondieron al tratamiento</span>
						<span class="hidden" data-i18n="graph_element2">OR&nbsp;=&nbsp;1,68 &lt;br/&gt;P&lt;0,05</span>
						<span class="hidden" data-i18n="graph_element3">(205 pacientes)</span>
						<span class="hidden" data-i18n="graph_element4">(263 pacientes)</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + LABA/ICS</span>
						<span class="hidden" data-i18n="graph_element6">Semana 48</span>
						<span class="hidden" data-i18n="graph_element7">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; + LABA/ICS</span>
						<span class="hidden" data-i18n="graph_element8">n=456</span>
						<span class="hidden" data-i18n="graph_element9">Probabilidad un 68&nbsp;% mayor de mejorar el control del asma&lt;sup&gt;3,4&lt;/sup&gt;</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2016, Figure 4.<sup>5</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">En un an&aacute;lisis conjunto de pacientes con asma sintom&aacute;tica en tratamiento con SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br&gt;5 &mu;g + &ge;800 &mu;g LABA/ICS, en la semana 48, la tasa de respuesta ACQ-7 fue significativamente mayor con SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;  + &ge;800 &mu;g LABA/ICS y, por lo tanto, la mejor&iacute;a de los s&iacute;ntomas del asma fue mayor en estos pacientes.</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_karen">&lt;p&gt;No solo tengo una sensaci&oacute;n diferente al tomar este medicamento complementario, sino que tambi&eacute;n me va mucho mejor en el trabajo, especialmente con las escaleras. Me alegro de haber podido dejar gradualmente los esteroides en comprimidos.&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Carina&lt;/strong&gt;&lt;br/&gt;
							32 a&ntilde;os
						&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_julia">&lt;p&gt;No es agradable sentir que te falta el aire, sobre todo si est&aacute;s sola. Con este medicamento complementario ya no me preocupa tanto la disnea. Siento que he recuperado un poco mi vida.&lt;/p&gt;
						&lt;p class="bottom"&gt;&lt;strong&gt;Julia&lt;/strong&gt;&lt;br/&gt;
						45 a&ntilde;os&lt;/p&gt;</div>
				</div>
				<button href="#exacerbations_04" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Evitar exacerbaciones</button>
			</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
