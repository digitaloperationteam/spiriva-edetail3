﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="stay-ahead_05" class="stay-ahead productPage" data-index="4" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S005" data-screen-label="Stay ahead" data-screen-name="Stay ahead" data-screen-section=""><div class="contentPage">
		<div class="col">
			<img id="imgProductPNG" border="0" src="images/img_product.gif"><div class="product_img_shadow">
				<img src="images/spiriva-shadow.png" alt="Spiriva shadow product"></div>
		</div>
		<div class="col">
			<h2 data-i18n="pageTitle">Helfen Sie Ihren Patienten&lt;br/&gt;&lt;span class="blue"&gt;R&uuml;ckenwind bei Asthma mit SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;/span&gt;</h2>
			<ul><li class="cloud"><span data-i18n="list_item1">Der Respimat&lt;sup&gt;&reg;&lt;/sup&gt; erzeugt eine feine, lang anhaltende Spr&uuml;hwolke aus kleinen Tr&ouml;pfchen, sodass der Wirkstoff tief in die Lunge Ihrer Patienten eindringen kann.&lt;sup&gt;1&lt;/sup&gt;</span>	
				</li>
				<li class="poumons"><span data-i18n="list_item2">Symptomatisches Asthma ist trotz t&auml;glicher Anwendung inhalativer Kortikosteroide/langwirksamer &beta;2-Agonisten h&auml;ufig.&lt;sup&gt;6&lt;/sup&gt; Diese Symptome k&ouml;nnen Patienten von k&ouml;rperlichen und anderen Alltagsaktivit&auml;ten abhalten. SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; kann Symptome verbessern und das Risiko schwerer Exazerbationen verringern. R&uuml;ckenwind bei Asthma.</span></li>
			</ul><p class="engUk-only">
				SPIRIVA&reg; Respimat&reg; is indicated as an add-on maintenance bronchodilator treatment in adult patients with asthma who are currently treated with the maintenance combination of inhaled corticosteroids (&ge;800 &mu;g budesonide/day or equivalent) and long-acting &beta;<sub>2</sub>-agonists<sup>5</sup> and who experienced one or more severe exacerbations in the previous year.
			</p>
			<div class="animated-button">
				<p data-i18n="paragraph1">&lt;strong&gt;Um SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; anzuwenden, m&uuml;ssen die Patienten den SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; nur aktivieren, aufklappen und ausl&ouml;sen.&lt;/strong&gt;</p>
				<button href="#how-to-use_06" class="goToSlide gtmClickNavigate" data-i18n="button_conclusion">Erfahren Sie mehr &uuml;ber SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;</button>
			</div>
		</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
