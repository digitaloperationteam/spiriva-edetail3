﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="exacerbations_04" class="helpPage" data-index="3" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S004" data-screen-label="Exacerbations" data-screen-name="Exacerbations" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br/&gt;&lt;span class="blue"&gt;reduzierte das Risiko schwerer Exazerbationen signifikant&lt;sup&gt;3,4&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; verz&ouml;gerte die Zeit bis zur ersten schweren Asthma-Exazerbation im Vergleich zur Kontrollgruppe signifikant&lt;sup&gt;3&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/exacerbationsGraph.png"><span class="hidden" data-i18n="graph_element1">Tage bis zur ersten schweren Asthma-Exazerbation</span>
						<span class="hidden" data-i18n="graph_element2">Hazard-Ratio: 0,79&lt;br/&gt; P=0,03</span>
						<span class="hidden" data-i18n="graph_element3">226&nbsp;Tage</span>
						<span class="hidden" data-i18n="graph_element4">282&nbsp;Tage</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element6">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;+ ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element7">n=454</span>
						<span class="hidden" data-i18n="graph_element8">Risikoreduktion um 21&nbsp;%&lt;sup&gt;3&lt;/sup&gt;</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2012, Figure 2C.<sup>4</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">Schwere Asthma-Exazerbationen waren im klinischen Pr&uuml;fplan folgenderma&szlig;en vordefiniert: s&auml;mtliche Asthma-Exazerbationen, die mindestens 3&nbsp;Tage lang mit systemischen (einschlie&szlig;lich oralen) Kortikosteroiden behandelt werden mussten, oder die &amp;mdash; bei bestehender oder vorbestehender Therapie mit systemischen Kortikosteroiden &amp;mdash; mindestens eine Verdopplung der bisherigen Tagesdosis systemischer Kortikosteroide f&uuml;r mindestens 3 Tage erforderten.&lt;sup&gt;3&lt;/sup&gt;</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_james">&lt;p&gt;Seit ich SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;anwende,&lt;br/&gt;
						kann ich sehr viel besser atmen.
						&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Tim&lt;/strong&gt;&lt;br/&gt;
							22 Jahre
						&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_sam">&lt;p&gt;Das neue Spray ist anders als die anderen Medikamente und ich habe schnell festgestellt, dass ich mein Bedarfsspray nicht mehr so oft ben&ouml;tige. Die feine Spr&uuml;hwolke macht es angenehmer zu inhalieren.  
						&lt;/p&gt;
						&lt;p class="bottom"&gt;
							&lt;strong&gt;Nina&lt;/strong&gt;&lt;br/&gt;
							30 Jahre
						&lt;/p&gt;</div>
				</div>
				<button href="#stay-ahead_05" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Was ist SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;?</button>
			</div>
	</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
