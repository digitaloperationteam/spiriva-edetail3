﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="symptoms_03" class="symptoms helpPage" data-index="2" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S003" data-screen-label="Symptoms" data-screen-name="Symptoms" data-screen-section=""><div class="contentPage">
			<div class="col">
				<h2 data-i18n="page_title">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;&lt;br/&gt;&lt;span class="blue"&gt;verbessert Asthma-Symptome&lt;sup&gt;3,4&lt;/sup&gt;&lt;/span&gt;</h2>
				<div class="scrollY">
					<p data-i18n="paragraph1">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; verbesserte Asthma-Symptome im Vergleich zur Kontrollgruppe&lt;sup&gt;3,4&lt;/sup&gt;</p>

					<p>
						<img border="0" src="images/master/symptomsGraph.png"><span class="hidden" data-i18n="graph_element1">% der Patienten mit Ansprechen</span>
						<span class="hidden" data-i18n="graph_element2">Odds-Ratio: 1,68&lt;br/&gt;P&lt;0,05</span>
						<span class="hidden" data-i18n="graph_element3">(205&nbsp;Patienten)</span>
						<span class="hidden" data-i18n="graph_element4">(263&nbsp;Patienten)</span>
						<span class="hidden" data-i18n="graph_element5">Placebo + ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element6">Woche&nbsp;48</span>
						<span class="hidden" data-i18n="graph_element7">SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;+ ICS/LABA</span>
						<span class="hidden" data-i18n="graph_element8">n=456</span>
						<span class="hidden" data-i18n="graph_element9">68 % h&ouml;here Wahrscheinlichkeit f&uuml;r eine Verbesserung der Asthma-Kontrolle&lt;sup&gt;3,4&lt;/sup&gt;</span>
					</p>
					<p class="text-ersOnly"><strong>This graph has been adapted from Kerstjens et al. 2016, Figure 4.<sup>5</sup></strong><br></p>
					<p class="small" data-i18n="paragraph2">In einer gepoolten Analyse symptomatischer Patienten, die 5 &mu;g SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; plus&lt;br&gt; &ge; 800 &mu;g ICS/LABA bekamen, war die ACQ-7-Responder-Rate in Woche 48 unter SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; + &ge; 800 &mu;g ICS/LABA signifikant h&ouml;her als unter Placebo + &ge; 800 &mu;g ICS/LABA, d. h., bei den Patienten hatten sich die Asthma-Symptome mit gr&ouml;&szlig;erer Wahrscheinlichkeit gebessert.</p>
				</div>
			</div>
			<div class="col">
				<div class="helpProfil first">
					<div class="quote" data-i18n="text_karen">&lt;p&gt;Diese zus&auml;tzliche Medizin gibt mir nicht nur ein anderes Gef&uuml;hl, wenn ich sie anwende, ich komme auch bei der Arbeit viel besser zurecht, insbesondere mit den Treppen. Und ich bin sehr froh, dass ich die Steroid-Tabletten langsam absetzen konnte.&lt;/p&gt;&lt;p class="bottom"&gt;
							&lt;strong&gt;Karen&lt;/strong&gt;&lt;br/&gt;
							32 Jahre&lt;/p&gt;</div>
				</div>
				<div class="helpProfil second">
					<div class="quote" data-i18n="text_julia">&lt;p&gt;Bei Atemnot habe ich Angst, insbesondere wenn ich alleine bin. Dieses zus&auml;tzliche Mittel hat mir geholfen, mir weniger Sorgen wegen der Atemnot zu machen. Ich habe das Gef&uuml;hl, als ob ich ein bisschen von meinem Leben zur&uuml;ckbekommen habe.&lt;/p&gt;
						&lt;p class="bottom"&gt;&lt;strong&gt;Julia&lt;/strong&gt;&lt;br/&gt;
						45 Jahre&lt;/p&gt;</div>
				</div>
				<button href="#exacerbations_04" class="absolute goToSlide gtmClickNavigate" data-i18n="button_next">Vermeidung von Exazerbationen</button>
			</div>
	</div>
	
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="halo1"></div>
	<div class="halo2"></div>
</section></body></html>
