﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="how-to-use_06" class="how-to-use" data-index="5" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S006" data-screen-label="How to use" data-screen-name="How to use" data-screen-section=""><div class="contentPage">

	<div class="col left-side">
		<div class="joiner-point"> 		
		</div>
		<div class="radial-inner">
		</div>
		<div class="radial-background"></div>
		<ul class="radial-slide-block"><li id="turn" class="use-element zoom-in">
				<p class="number">1</p>
				<div class="slide-info" style="opacity:1; display: block;">
					<p data-i18n="how_to_use1">AKTIVIEREN</p>
					<p data-i18n="how_to_use2"></p>
					<p data-i18n="how_to_use3">Geh&auml;useunterteil in Pfeilrichtung bis zum h&ouml;rbaren Klick drehen.&lt;sup&gt;5&lt;/sup&gt;</p>
				</div>
			</li>
			<li id="open" class="use-element active">
				<p class="number">2</p>
				<div class="slide-info">
					<p data-i18n="how_to_use4">AUFKLAPPEN</p>
					<p data-i18n="how_to_use5"></p>
					<p data-i18n="how_to_use6">Schutzkappe bis zum Anschlag &ouml;ffnen.&lt;sup&gt;5&lt;/sup&gt;</p>
				</div>
			</li>
			<li id="press" class="use-element active">
					<p class="number">3</p>
				<div class="slide-info">
					<p data-i18n="how_to_use7">AUSL&Ouml;SEN</p>
					<p data-i18n="how_to_use8"></p>
					<p data-i18n="how_to_use9">Atmen Sie langsam und vollst&auml;ndig aus.&lt;br&gt;
Umschlie&szlig;en Sie das Mundst&uuml;ck mit den Lippen. Dr&uuml;cken Sie den Ausl&ouml;seknopf und atmen Sie gleichzeitig langsam und tief ein.&lt;sup&gt;5&lt;/sup&gt;</p>
				</div>
			</li>
			<li id="repeat" class="use-element active">
					<p class="number">4</p>
				<div class="slide-info">
					<p data-i18n="how_to_use10">WIEDERHOLEN</p>
					<p data-i18n="how_to_use11">Aktivieren &ndash; Aufklappen &ndash; Ausl&ouml;sen</p>
					<p data-i18n="how_to_use12">Einmal t&auml;glich, 2 H&uuml;be.</p>
				</div>
			</li>

		</ul></div>

	 <div class="col right-side">
	 	<h2 data-i18n="how_to_use_title">T&auml;gliche Anwendung von SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;:&lt;br/&gt;&lt;span class="blue"&gt;Aktivieren &ndash; Aufklappen &ndash; Ausl&ouml;sen&lt;/span&gt;</h2>
	 	<p data-i18n="how_to_use_paragraph">Bitte sorgen Sie daf&uuml;r, dass Ihre Patienten den Respimat&lt;sup&gt;&reg;&lt;/sup&gt; richtig anwenden. Informieren Sie Ihre Patienten, dass der Respimat&lt;sup&gt;&reg;&lt;/sup&gt; wie folgt angewendet werden muss:</p>
	 	<button data-i18n="how_to_use-button" href="#learn-more_06" class="goToSlide gtmClickNavigate">Erfahren Sie mehr</button>
	 </div>

</div>

	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>

</section></body></html>
