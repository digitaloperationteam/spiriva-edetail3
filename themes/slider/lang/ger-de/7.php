﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="learn-more_06" class="learn-more" data-index="6" data-transition="none" data-background="images/bgd2.jpg" data-background-transition="fadeIn" data-screen-id="2016_SPI_ED3_S007" data-screen-label="Learn more" data-screen-name="Learn more" data-screen-section=""><div class="contentPage">
		<div class="title-anim">
			<h2 data-i18n="page_title">M&ouml;chten Sie mehr dar&uuml;ber erfahren, wie&lt;br/&gt;&lt;span class="blue"&gt;SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; Ihren Asthma-Patienten helfen kann?</h2>
			<div class="questions">
				<div class="col">
					<h3 data-i18n="title1">Wo finde ich weitere Informationen &uuml;ber den Inhalator Respimat&lt;sup&gt;&reg;&lt;/sup&gt;?</h3>
				</div>
				<div class="col">
					<h3 data-i18n="title2">Welchen weiteren Nutzen hat &lt;br/&gt;SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt; f&uuml;r Asthma-Patienten?</h3>
				</div>
			</div>
		</div>
		<div id="bubbleBlock">
			<div class="col firstBubble">
				<a class="gtmClickExternalLink" data-gtm="Product informations" data-i18n="[href]find_out_btn" href="" target="_blank">
					<span data-i18n="find_out_button">Hier erfahren Sie mehr</span>
					<img src="image/find-product.png" alt="find product"></a>
				<p class="ersOnly">Case description for educational purposes; not real patient case<br><br>
				Summary of Product Characteristics available at the booth.<br>
				Submitted to AIFA on 01/08/2017</p>
			</div>
			<div class="col secondBubble">
				<a target="_blank" class="jamesImage gtmClickExternalLink" data-link-type="minisite" data-gtm="James-image" href="spiriva/edetail1;#/james_02">
					<img src="image/meet-james.png" alt="meet-james"></a>
				<a target="_blank" class="karenImage gtmClickExternalLink" data-link-type="minisite" data-gtm="Karen-image" href="spiriva/edetail1;#/karen_03">
					<img src="image/meet-karen.png" alt="meet-karen"></a>
				<a target="_blank" class="jamesButton button gtmClickExternalLink" data-link-type="minisite" data-gtm="James-button" href="spiriva/edetail1;#/james_02" data-i18n="meet_james_button">Tim</a>
				<a target="_blank" class="karenButton button gtmClickExternalLink" data-link-type="minisite" data-gtm="Karen-button" href="spiriva/edetail1;#/karen_03" data-i18n="meet_karen_button">Karen</a>
			</div>
			<div class="col thirdBubble">
				<a target="_blank" class="juliaImage gtmClickExternalLink" data-gtm="Julia-image" data-link-type="minisite" href="spiriva/edetail2;#/julia_03">
					<img src="image/meet-julia.png" alt="meet-julia"></a>
				<a target="_blank" class="samImage gtmClickExternalLink" data-gtm="Sam-image" data-link-type="minisite" href="spiriva/edetail2;#/sam_02">
					<img src="image/meet-sam.png" alt="meet-sam"></a>
				<a target="_blank" class="juliaButton button gtmClickExternalLink" data-gtm="Julia-button" data-link-type="minisite" href="spiriva/edetail2;#/julia_03" data-i18n="meet_julia_button">Julia</a>
				<a target="_blank" class="samButton button gtmClickExternalLink" data-gtm="Sam-button" data-link-type="minisite" href="spiriva/edetail2;#/sam_02" data-i18n="meet_sam_button">Nina</a>
			</div>
		</div>
	</div>
	<div class="jobCode espEsOnly">
		SPI.1584.072017 - Boehringer Ingelheim Espa&ntilde;a, S.A. - Fecha de preparaci&oacute;n: Julio 2017
	</div>
	<div class="bgdBubble big first">
	</div>
	<div class="bgdBubble big second">
	</div>
	<div class="bgdBubble third">
	</div>
	<div class="bgdBubble fourth">
	</div>
	<div class="bgdBubble small">
	</div>

	<div class="lightbox" id="referencesPopin">
		<h2 data-i18n="title_popin_1">Referenzen</h2>
		<ol><li data-i18n="list_item_popin1">Anderson P. Use of Respimat&lt;sup&gt;&reg;&lt;/sup&gt; Soft Mist&lt;sup&gt;&#153;&lt;/sup&gt; Inhaler in COPD patients. Int J COPD. 2006;1(3):251-9.</li>
			<li data-i18n="list_item_popin2">Pitcairn G, Reader S, Pavia D, Newman S. Deposition of corticosteroid aerosol in the human lung by Respimat Soft Mist inhaler compared to deposition by metered dose inhaler or by Turbuhaler dry powder inhaler. J Aerosol Med. 2005; 18(3):264-72.</li>
			<li data-i18n="list_item_popin3">Kerstjens H, Engel M, Dahl R, et al. Tiotropium in asthma poorly controlled with standard combination therapy. N Engl J Med 2012;367(13):1198-1207.</li>
			<li data-i18n="list_item_popin4"></li>
			<li data-i18n="list_item_popin5">Fachinformation SPIRIVA&lt;sup&gt;&reg;&lt;/sup&gt; Respimat&lt;sup&gt;&reg;&lt;/sup&gt;, Stand Feb.
2016</li>
			<li data-i18n="list_item_popin6">Demoly P, Annunziata K, Gubba E et al. Repeated cross-sectional survey of patient-reported asthma control in Europe in the past 5 years. Eur Respir Rev 2012;21(123):66-74.</li>
			<li class="engGlo-only" data-i18n="list_item_popin7">Dahl R, Engel M, Dusser D, et al. Safety and tolerability of once-daily tiotropium respimat&lt;sup&gt;&reg;&lt;/sup&gt; as add-on to at least inhaled corticosteroids in adult patients with symptomatic asthma: A pooled safety analysis. Respir Med. 2016;118:102-111.</li>
			<li class="hidden engUk-only">Global Initiative for Asthma. Pocket guide for asthma management and prevention (updated 2016). Accessed October 2017</li>
			<li class="ersOnly">Placebo Respimat<sup>&reg;</sup> inhaler package leaflet; instructions for demonstration. Boehringer Ingelheim International GmbH. July 2016</li>
		</ol></div>
	<div class="lightbox" id="prescribingPopin">
		<h2 data-i18n="title_popin_2"></h2>
		<p data-i18n="text1_popin_2"></p>
		<p data-i18n="text2_popin_2"></p>
		<p data-i18n="text3_popin_2"></p>

		<div class="lightbox-button">
			<a class="button gtmClickDownload" href="" data-gtm="PI" data-i18n="btn_popin_learn_more;[href]btn_popin_learn_more_url"></a>
		</div>

	</div>
</section></body></html>
