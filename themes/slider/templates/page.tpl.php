<nav>
  <div class="contentNav">
    <div class="logoNav">
      <a href="#home_01" class="goToSlide">
      &nbsp;
      </a>
    </div>
    <?php if ($aptushealth_menu): ?>
   		<?php print render($aptushealth_menu); ?>
   	<?php endif; ?>
  </div>
</nav>

<div class="pathNav">
  <ul class="circle-container">
    <li class="gtmClickNavigate active" data-index="0"><span></span></li>
    <li class="gtmClickNavigate" data-index="1"><span></span></li>
    <li class="gtmClickNavigate" data-index="2"><span></span></li>
    <li class="gtmClickNavigate" data-index="3"><span></span></li>
    <li class="gtmClickNavigate" data-index="4"><span></span></li>
    <li class="gtmClickNavigate" data-index="5"><span></span></li>
    <li class="gtmClickNavigate" data-index="6"><span></span></li>
  </ul>
</div>

<div class="reveal">
<!-- Any section element inside of this container is displayed as a slide -->
  <div class="slides">
    <?php echo $slides; ?>
  </div>
</div>
