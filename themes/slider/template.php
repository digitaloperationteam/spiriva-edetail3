<?php

function slider_preprocess_page(&$variables) {
  global $user;

  $theme_path = path_to_theme();
  if ($user->uid == 0 && arg(0) == 'user') {
    drupal_add_css($theme_path . '/css/login.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  } else {
    // Add your css
    drupal_add_css($theme_path . '/app/css/main.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
    drupal_add_css($theme_path . '/app/css/circle.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
    drupal_add_css('http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css', array('group' => CSS_THEME, 'type' => 'external', 'preprocess' => FALSE));
    drupal_add_css($theme_path . '/app/css/reveal.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
    drupal_add_css($theme_path . '/app/css/featherlight.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
    drupal_add_css($theme_path . '/app/css/theme/black.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
    drupal_add_css($theme_path . '/app/css/jquery.mCustomScrollbar.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));

    $html5shiv = array(
      '#tag' => 'script',
      '#attributes' => array( // Set up an array of attributes inside the tag
        'src' => $server_url . '/' . $site_name . '/' . $theme_path . '/player/js/html5shiv.js',
      ),
      '#prefix' => '<!--[if lte IE 9]>',
      '#suffix' => '</script><![endif]-->',
    );
    drupal_add_html_head($html5shiv, 'html5shiv');
    // Add yout js
    drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 10));
    drupal_add_js('https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 10));
    drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 10));
    drupal_add_js($theme_path . '/player/js/head.min.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app/js/reveal.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app/js/main.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app/js/gsapAnimation.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app/js/pathNav.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app//js/featherlight.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app/js/config_player.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
    drupal_add_js($theme_path . '/app/js/jquery.mCustomScrollbar.concat.min.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
  }
}

function slider_process_page(&$variables) {
  global $language;

  if ('master' == $language->language) {
    $aptushealth_menu_tree = menu_tree_all_data('main-menu');
    $variables['aptushealth_menu'] = menu_tree_output($aptushealth_menu_tree);
  } else {
    $aptushealth_menu_tree = menu_tree_all_data('menu-main-menu-' . $language->language);
    $variables['aptushealth_menu'] = menu_tree_output($aptushealth_menu_tree);
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function slider_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  $theme_path = path_to_theme();
  drupal_add_css($theme_path . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function slider_process_maintenance_page(&$variables) {
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

