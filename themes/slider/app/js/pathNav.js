Reveal.addEventListener( 'slidechanged', function ( event ) {
	var navPath = jQuery('.pathNav ul'),
		currentSlide = jQuery(event.currentSlide),
		currentSlideIndex = currentSlide.attr('data-index'),
		navRotation = -30 * currentSlideIndex;

	jQuery('.pathNav ul li').removeClass('active');
	TweenMax.to(navPath, 2, {rotation:navRotation, ease:Elastic.easeOut.config(1, 1), delay:0.2});
	jQuery('.pathNav ul li[data-index="'+currentSlideIndex+'"]').addClass('active');
});

jQuery('.pathNav ul li').on('click', function(){
	var target = jQuery(this).attr('data-index');
	pathNavGoToSlide(target);
});

//Functions
function pathNavGoToSlide(target){
	var indices = Reveal.getIndices(jQuery('section[data-index="'+ target +'"]')[0]);
	Reveal.slide(indices.h);
	return false;
}