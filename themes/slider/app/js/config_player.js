// More info https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({
    controls: false,
    progress: false,
    history: true,
    center: true,
    touch: false,
    overview: false,

    transition: 'fade', // none/fade/slide/convex/concave/zoom

    // More info https://github.com/hakimel/reveal.js#dependencies
    dependencies: [
        { src: '../sites/spiriva-edetail1/themes/aptushealthasset/player/js/classList.js', condition: function() { return !document.body.classList; } },
        { src: '../sites/spiriva-edetail1/themes/aptushealthasset/player/js//plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
        { src: '../sites/spiriva-edetail1/themes/aptushealthasset/player/js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
        { src: '../sites/spiriva-edetail1/themes/aptushealthasset/player/js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
        { src: '../sites/spiriva-edetail1/themes/aptushealthasset/player/js/plugin/zoom-js/zoom.js', async: true },
        { src: '../sites/spiriva-edetail1/themes/aptushealthasset/player/js/plugin/notes/notes.js', async: true }
    ]
});