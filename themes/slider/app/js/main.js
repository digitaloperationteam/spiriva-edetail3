jQuery(document).ready(function() {
    //Variables

    var seeDataButton = jQuery('.jsSeeData'),
        seeVisualButton = jQuery('.jsSeeVisual'),
        answerData = jQuery('#answer'),
        answerDataERS = jQuery('body.i18n-ers-eu #answer'),
        lungSystem = jQuery('.lungSystem'),
        buttonSet2 = jQuery('.buttonSet2'),
        buttonSet1 = jQuery('.buttonSet1'),
        sentence = jQuery('.hiden-on-answer');

    //Eventlistner
    jQuery('.goToSlide').on('click', goToSlide);

    //Functions
    function goToSlide(){
    	
    	var target = jQuery(this).attr('href'),
    	 	indices = Reveal.getIndices(jQuery(target)[0]);


    	Reveal.slide(indices.h);
    	return false;
    }


    function checkIphone(){
        jQuery('body').css('padding-top', '0px');
        if ( ("standalone" in window.navigator) && !window.navigator.standalone){
         if(window.innerHeight < window.innerWidth){
            jQuery('body').css('padding-top', '45px');
         }
        }
    }

    window.addEventListener("orientationchange", function() {
        checkIphone();
    });


    //Lung animation - Quizz

    var steps = [
        {
            class : '.step2',
            start : 0,
            end : 0.089
        },
        {
            class : '.step3',
            start : 0.089,
            end : 0.285
        },
        {
            class : '.step4',
            start : 0.285,
            end : 0.40
        },
        {
            class : '.step6',
            start : 0.40,
            end : 0.60
        } ,
        {
            class : '.step7',
            start : 0.60,
            end : 0.70
        } ,
        {
            class : '.step8',
            start : 0.70,
            end : 0.80
        } ,
        {
            class : '.step9',
            start : 0.80,
            end : 0.90
        } ,
        {
            class : '.step10',
            start : 0.90,
            end : 1
        }  
    ];

    jQuery(".slider").slider({
        range: "min",
        value: 0,
        min: 0,
        max: 1,
        step: 0.001,
        create: function(event, ui){
            jQuery('.ui-slider-handle').append('<span class="cursor">0</span>');
        },
        slide: function (event, ui) {
            var slider = jQuery(this);
            var lungTarget = slider.data('target'),
                lungTarget = jQuery('#'+lungTarget);
            
            updateImage(slider, ui.value);
        }
    });


/*** 
        FUNCTION TO UPDATE THE LUNG IMAGE
        used when the user slide the cursor and 
        when he check the answer.
***/  
function updateImage(slider, cursorPosition, checkAnswer, goodAnswerImage){
    var lungTarget = slider.data('target'),
        lungTarget = jQuery('#'+lungTarget);

    jQuery.each(steps, function(i, value){
        if((cursorPosition >= steps[i].start) && (cursorPosition <= steps[i].end)){

            var valOpacity = calcOpacity(steps[i].start, steps[i].end, cursorPosition);

            //console.log(steps[i].class);

            lungTarget.find(steps[i].class).css('opacity', valOpacity);
            lungTarget.find(steps[i].class+'+span').css('opacity', 0);

            lungTarget.find('.circleProgress').removeClass(function (index, className) {
                return (className.match (/(^|\s)pourcentage-\S+/g) || []).join(' ');
            });

            //Circle progress bar
            var pourcentageCompletion = cursorPosition*100;
                roundedPourcentageCompletion = Math.round(pourcentageCompletion);
            lungTarget.find('.circleProgress').addClass('pourcentage-'+roundedPourcentageCompletion);

            pourcentageCompletion = pourcentageCompletion.toFixed(1);

            slider.find('.ui-slider-handle .cursor').empty().append(pourcentageCompletion);

        }
        
    });

    if(checkAnswer){
        lungTarget.find('.lung').css('opacity', 0);
    }

}

    function checkAnswer(){

        jQuery(".checkAnswer").click(function() {
            var firstResponse = jQuery('#sliderLung1 .ui-slider-handle').find('.cursor:first').text(),
                secondResponse = jQuery('#sliderLung2 .ui-slider-handle').find('.cursor:first').text(),
                thirdResponse = jQuery('#sliderLung3 .ui-slider-handle').find('.cursor:first').text();

           if(jQuery('body.i18n-eng-uk').length != 0){
                var checkResult = [
                        {
                            "answer" : firstResponse,
                            "result" : 8.9, 
                            "image" :  'step2_8-9.jpg'     
                        },
                        {
                            "answer" : secondResponse,
                            "result" : 28.5, 
                            "image" :  'step3_28-5.jpg'  
                        },
                        {
                            "answer" : thirdResponse,
                            "result" : 51.6, 
                            "image" :  'step7_51.jpg'  
                        }
                ];
            }else{
                var checkResult = [
                        {
                            "answer" : firstResponse,
                            "result" : 9, 
                            "image" :  'step2_8-9.jpg'     
                        },
                        {
                            "answer" : secondResponse,
                            "result" : 29, 
                            "image" :  'step3_28-5.jpg'  
                        },
                        {
                            "answer" : thirdResponse,
                            "result" : 52, 
                            "image" :  'step7_51.jpg'  
                        }
                ];
            }

            jQuery.each(checkResult, function(i, value){
                var j = i + 1;

                jQuery("#sliderLung" + j).slider({
                  value: checkResult[i].result / 100
                });

                //jQuery("#contentLung" + j).find('.ui-slider-handle').css("left" , checkResult[i].result + '%' );
                jQuery("#contentLung" + j).addClass('seeAnswer');
                jQuery("#contentLung" + j).find('.ui-slider-handle').children().text(checkResult[i].result);

                var falseAnswer = "<div class='falseAnswer' style='left:" + checkResult[i].answer + "%'>" + checkResult[i].answer + "%<div>&nbsp;</div></div>";

                if(checkResult[i].result != checkResult[i].answer){

                    jQuery("#contentLung" + j).find('.slider').append(falseAnswer);

                }
            });

            jQuery('.slider').each(function(i){
                var slider = jQuery(this),
                    lungTarget = slider.data('target'),
                    lungTarget = jQuery('#'+lungTarget),
                    correctAnswer = checkResult[i].result / 100;
                    

                updateImage(slider, correctAnswer, true);
            });

            jQuery('.slider').slider({
                disabled: true
            });


            jQuery(".cursor").addClass("cursorGreen");

            seeDataButton.removeClass("hidden");
            seeDataButton.addClass("active");
            buttonSet2.removeClass('hidden');
            buttonSet1.addClass('hidden');
            
        });
     }
        sentence.removeClass("hidden");
        jQuery('.hide-onlyfor-ers').addClass('hidden');
        jQuery('.onlygraph').addClass('hidden');

     function displayData() {
        seeDataButton.click(function() {
            answerData.removeClass('hidden');
            answerDataERS.removeClass('hidden');
            jQuery('.nograph').addClass('hidden');
            jQuery('.onlygraph').removeClass('hidden');
            lungSystem.addClass('hidden');
            jQuery(this).addClass("hidden");
            jQuery(this).removeClass("active");
            seeVisualButton.removeClass("hidden");
            sentence.addClass("hidden");
            jQuery('.hide-onlyfor-ers').removeClass('hidden');
        })

        seeVisualButton.click(function() {
            sentence.addClass("hidden");
            answerData.addClass('hidden');
            answerDataERS.addClass('hidden');
            lungSystem.removeClass('hidden');
            jQuery('.nograph').removeClass('hidden');
            jQuery('.onlygraph').addClass('hidden');
            seeDataButton.removeClass('hidden');
            jQuery(this).addClass("hidden");
            jQuery('.hide-onlyfor-ers').addClass('hidden');
        })
     }

     displayData();
     checkAnswer();
     
    function calcOpacity(X, Y, uivalue){
        /****
            a*step1+b = 0;
            a*step2+b = 1;
        *****/
        var a,
        b, 
        valOpacity;

        a = -1/(X-Y);
        b = -(-X/(X-Y));
        value = a*uivalue + b;

        return value;
    }

// Radial slider

    var intervalTurn; 

    jQuery(".use-element").click(function (){
        clearInterval(intervalTurn);
        turnSlider(jQuery(this));
    });


    Reveal.addEventListener( 'slidechanged', function ( event ) {

        var currentSlide = jQuery(event.currentSlide);

            if(currentSlide.hasClass('how-to-use')) {
                resetSlider();
            }
        infiniteTurn();
    });



    function resetSlider(){
        var slider = jQuery(".radial-slide-block"),
            firstSlide = jQuery('#turn');

         UpdateDegree = 0,
         UpdateminusDegree = 0;

        //Reset all elements
        slider.css('transform', 'rotate(0)');
        slider.find('li').addClass('active').css('transform', 'rotate(0)');
        slider.find('li').removeClass("zoom-in");
        slider.find(".slide-info").css("opacity" , "0");
        slider.find(".slide-info").css("display" , "none");

        //Show the first element "#turn"
        firstSlide.removeClass('active');
        firstSlide.addClass("zoom-in");
        firstSlide.find(".slide-info").css("opacity" , "1");
        firstSlide.find(".slide-info").css("display" , "block");


        
    }

    function getDistance(elem){

            var fixedPoint = jQuery('.joiner-point');
            var clickedElement = elem;
            newDistance = fixedPoint.offset().top - clickedElement.offset().top;
            return newDistance;
    }

    var UpdateDegree = 0;
    var UpdateminusDegree = 0;


    
    function infiniteTurn(){
        clearInterval(intervalTurn);

        intervalTurn = setInterval(
        function(){
            var el = jQuery('.use-element').not('.active').next();
            if(el.length == 0){
                el = jQuery('.use-element').first();
            }

            turnSlider(el); 
        }, 6000);
    }

    
    function turnSlider(element){
        if (element.hasClass("active")){
                var slider = jQuery(".radial-slide-block"),
                    clickedElement = element,
                    elementId = element.attr('id'),
                    elementNotClicked = [];


                jQuery('.use-element.active').not('#' + elementId).each(
                    function(i){
                        elementNotClicked[i] = jQuery(this).offset().top;
                    }
                );

                jQuery(".use-element").addClass("active");
                clickedElement.removeClass("active");

                var lowerElement = Math.min(element.offset().top, elementNotClicked[0], elementNotClicked[1]),
                    higherElement = Math.max(element.offset().top, elementNotClicked[0], elementNotClicked[1]);

                var degree = 0;

                if(element.offset().top == lowerElement){
                    degree = 90;
                }else if(element.offset().top == higherElement) {
                    degree = 270;

                }else{
                    degree = 180;
                }


                jQuery({deg: UpdateDegree}).animate({deg: UpdateDegree + degree}, {
                    duration: 1300,
                    step: function(now) {
                        slider.css({
                            transform: 'rotate(' + now + 'deg)'
                        });
                    }
                })

                jQuery({deg: UpdateminusDegree}).animate({deg: UpdateminusDegree - degree}, {
                    duration: 1300,
                    step: function(now) {
                        jQuery(".use-element").css({
                            transform: 'rotate(' + now + 'deg)'
                        });
                    }
                })


                if ( jQuery(".use-element").hasClass( "zoom-in" ) ){
                    jQuery(".use-element").removeClass("zoom-in");
                }
                clickedElement.addClass("zoom-in");

                UpdateDegree = UpdateDegree+degree;
                UpdateminusDegree = UpdateminusDegree-degree;

                jQuery(".use-element").find(".slide-info").css("opacity" , "0");
                jQuery(".use-element").find(".slide-info").css("display" , "none");

                description = clickedElement.find(".slide-info");

                setTimeout (function (){
                    description.css("display" , "block");
                    description.animate({
                        opacity : 1
                    },600)

                }, 1000);

            }else{
                console.log("Pas actif");
            }

    }

    jQuery("body.i18n-eng-uk .scrollY, body.i18n-esp-es .scrollY, body.i18n-ers-eu .scrollY").mCustomScrollbar({
        axis:"y", // horizontal scrollbar
        theme:"spiriva"
      });


    //GER - DE LOCAL
    jQuery("body.i18n-ger-de .firstBubble a").attr('href','https://www.respimat.de/respimat/anwendung.htm');
    //jQuery("body.i18n-ger-de .thirdBubble a").on('click', function(){
        //return false;
    //});

    //jQuery("body.i18n-ger-de .thirdBubble a.button").css('background', '#cccccc')
    //jQuery("body.i18n-ger-de .thirdBubble a.button").css('opacity', '0.85')



});