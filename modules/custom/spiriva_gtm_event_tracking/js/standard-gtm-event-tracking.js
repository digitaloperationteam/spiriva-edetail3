/*******************
Google Tag Manager : Events & DataLayer

Author: LOPEZ Pierre-Yves
Version: 1.0.0
Company: Aptus Health
********************/

jQuery(document).ready(function() {

		/*******
		Eventlistener : onclick
		********/
		jQuery('*[class*="gtmClick"]').on('click', function(){

			var classes = jQuery(this).attr('class').split(' ');
			var dataGtm = jQuery(this).data('gtm');
			var eventName,
				eventLabel = '';

			jQuery.each(classes, function(i, val){
				if(val.indexOf("gtmClick") != -1){
					eventName = val;
				}
			});

			if(dataGtm){ eventLabel = dataGtm;}

			dataLayer.push({
				'event': eventName,
				'eventLabel': eventLabel
			});
		});

		/*******
		Eventlistener : swipe
		********/
		jQuery('*[class*="gtmSwipe"]').on('swipe', function(){

			var classes = jQuery(this).attr('class').split(' ');
			var dataGtm = jQuery(this).data('gtm');
			var eventName,
				eventLabel = '';

			jQuery.each(classes, function(i, val){
				if(val.indexOf("gtmSwipe") != -1){
					eventName = val;
				}
			});
			if(dataGtm){ eventLabel = dataGtm;}
			
			dataLayer.push({
				'event': eventName,
				'eventLabel': eventLabel
			});
		});

		/*******
		Eventlistener : keydown
		********/
		var arrayKeyCode = [
			{
				name: 'NavigateKeyPrev',
				code: 37
			},
			{
				name: 'NavigateKeyNext',
				code: 39
			}
		];

		jQuery('body').keydown(function(e){
			jQuery.each(arrayKeyCode, function(i, val){
				if(e.keyCode == val.code){
					dataLayer.push({'event': 'gtmKeydownNavigate' });
				}
			});
		});

});
