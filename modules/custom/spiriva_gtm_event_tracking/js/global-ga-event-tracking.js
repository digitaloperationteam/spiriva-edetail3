jQuery(document).ready(function() {

	jQuery.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = jQuery(this).data();
            var tc = data.toggleclicked;
            jQuery.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };

	Reveal.addEventListener( 'ready', function( event ) {
		pushDataLayer();
		dataLayer.push({'event': 'gaReady'});
		
		jQuery('.navigate-left, .navigate-prev, .progress a.slideButton.past').click(function(){
			pushDataLayer();
			dataLayer.push({'event': 'gaNavigatePrev'});
		});
		jQuery('.navigate-right, .navigate-next').click(function(){
			pushDataLayer();
			dataLayer.push({'event': 'gaNavigateNext'});
		});
		jQuery('.reveal').on("swipe",function(){
			pushDataLayer();
			dataLayer.push({'event': 'gaSwipe'});
		});
		jQuery('body').keydown(function(e){
			if (e.keyCode == 37) {
				setTimeout(function(){
					pushDataLayer();
					dataLayer.push({'event': 'gaNavigateKeyPrev'});
				}, 20);
			} else if (e.keyCode == 39) {
				setTimeout(function(){
					pushDataLayer();
					dataLayer.push({'event': 'gaNavigateKeyNext'});
				}, 20);
			}
		});
		jQuery('a.menuButton').click(function(){
			setTimeout(function(){
	            pushDataLayer();
	            dataLayer.push({'event': 'gaNavigateMenu'});
	        }, 20);
        });
        jQuery('a.pageLink').click(function(){
        	setTimeout(function(){
            	pushDataLayer();
            	dataLayer.push({'event': 'gaSlideChange'});
            }, 20);
        });
 
	});

	
	function pushDataLayer(){
		var slide_active = '';
		var screen_name = '';
		var screen_id = '';
		var screen_section = '';
		var screen_number = '';
		var screen_metadata_1 = '';
		var screen_metadata_2 = '';
		var screen_metadata_3 = '';
		var screen_metadata_4 = '';
		var screen_metadata_5 = '';

		currentSlide = Reveal.getCurrentSlide().id;
		slide_active = jQuery('#' + currentSlide);
		screen_name = slide_active.data('screen-name');
		screen_id = slide_active.data('screen-id');
		screen_section = slide_active.data('screen-section');
		screen_number = slide_active.data('screen-number');
		screen_metadata_1 = slide_active.data('screenmetadata1');
		screen_metadata_2 = slide_active.data('screenmetadata2');
		screen_metadata_3 = slide_active.data('screenmetadata3');
		screen_metadata_4 = slide_active.data('screenmetadata4');
		screen_metadata_5 = slide_active.data('screenmetadata5');
		
		dataLayer.push({'screenName': screen_name });
		dataLayer.push({'screenSection': screen_section });
		dataLayer.push({'screenID': screen_id });
		dataLayer.push({'screenNumber': screen_number });
		dataLayer.push({'screenMetadata1': screen_metadata_1 });
		dataLayer.push({'screenMetadata2': screen_metadata_2 });
		dataLayer.push({'screenMetadata3': screen_metadata_3 });
		dataLayer.push({'screenMetadata4': screen_metadata_4 });
		dataLayer.push({'screenMetadata5': screen_metadata_5 });
	}
});
